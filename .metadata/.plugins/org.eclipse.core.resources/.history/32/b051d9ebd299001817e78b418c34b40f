/*
 * controller.c
 *
 *  Created on: 19 jul. 2018
 *      Author: elgarbe
 */
#include "usbd_cdc_if.h"
#include "controller.h"
#include "spektrum.h"
#include "PWM.h"
#include "math.h"
#include "usart.h"
#include "tim.h"

extern float pitch_compfilt;
extern float roll_compfilt;
extern float yaw_compfilt;

extern float pitch_g;
extern float roll_g;
extern float yaw_g;
extern float pitch_a;
extern float roll_a;

extern float GYR_X;
extern float GYR_Y;
extern float GYR_Z;

uint8_t cnt_Matlab=0;

FunctionalState CONTROL_CALCULATE = DISABLE;

//Spektrum controller
uint32_t Spektrum_Data[6]={0,0,0,0,0,0};
uint16_t Spektrum_pitch=0;
uint16_t Spektrum_roll=0;
uint16_t Spektrum_yaw=0;
uint16_t Spektrum_thrust=0;
uint16_t Spektrum_switch=0;
uint16_t Spektrum_height=0;
FunctionalState Bench_test = DISABLE;
float yaw_total;

//Controller references
float Spektrum_angles_ref[3]={0,0,0};
//static float angles_ref[3]={0,0,0};
static float angle_pitch_ref, angle_roll_ref, angle_yaw_ref;
float gz_ref=0;

//Variables with main use inside controller but modified sometimes in external functions
float Z0_joystick=0;

//PID Pitch-Roll-Yaw
float Kp=1.0;		//prop angle
float Ki=0.0;		//int angle
float Kd=1.1;		//diff (prop gyro)
//float Kd=0.5;		//diff (prop gyro)

//float K_PHI=0.0;	//int gyro
//float K_DIF=0.00;	//diff gyro

float Kp_Y=0.5;
float Ki_Y=0.01;
float Kd_Y=0.0;

int8_t cadena[100];
//uint8_t * p_data;

struct send_data
{
	uint8_t prefix1;
	uint8_t prefix2;
	uint8_t prefix3;
	uint8_t prefix4;
	float p_est;
    float p_ref;
    float p_err;
    float p_ein;
	float p_ede;
	float r_est;
    float r_ref;
    float r_err;
    float r_ein;
	float r_ede;
	float y_est;
    float y_ref;
    float y_err;
    float y_ein;
	float y_ede;
	float Mx;
	float My;
	float Mz;
	float PWM1_f;
	float PWM2_f;
	float PWM3_f;
	float PWM4_f;
	float PWM1;
	float PWM2;
	float PWM3;
	float PWM4;
	float Kp;
	float Ki;
	float Kd;
    uint8_t subfix1;
    uint8_t subfix2;
    uint8_t subfix3;
    uint8_t subfix4;
};

struct send_data str_cadena;

void CONTROL_PID(void)
{


	if (CONTROL_CALCULATE==ENABLE) {
		uint16_t PWM1_duty, PWM2_duty, PWM3_duty, PWM4_duty;

		static uint32_t t_ini_spektrum, t_end_spektrum, t_betw_spektrum;
		static float pitch_ref_last=0, roll_ref_last=0;
		static float pitch_ref_der, roll_ref_der;

		float RX_STATUS;
		//Moments in each axis
		float Mx, My, Mz;


		//========================================================================================================================================
		//========================================================================================================================================
		//pinMQ Matrix: moments to torque; ZLMN: moments vector; torques in each motor
		//====================================================================
		//Quadcopter GPSIC, kt=0.03
		static float pinMQ[4][4]={	{0.2500,   -1.4142,    1.4142,    8.3333},
									{0.2500,    1.4142,    1.4142,   -8.3333},
									{0.2500,    1.4142,   -1.4142,    8.3333},
									{0.2500,   -1.4142,   -1.4142,   -8.3333}};



		//========================================================================================================================================
		//========================================================================================================================================
		static float ZLMN[4]={0, 0, 0, 0};
		float PWM1_f, PWM2_f, PWM3_f, PWM4_f;

		//Estimated angles using gyros integral and accel data with different weights
		static float angle_pitch_estim=0, angle_roll_estim=0, angle_yaw_estim=0;

		static float gx_filtered=0, gy_filtered=0, gz_filtered=0;
//		static float gx_filtered_last=0, gy_filtered_last=0, gz_filtered_last=0;

		float angle_pitch_error, angle_roll_error, angle_yaw_error;

//		static float angle_pitch_error_prev=0, angle_roll_error_prev=0, angle_yaw_error_prev=0;
		static float angle_pitch_error_integral=0, angle_roll_error_integral=0, angle_yaw_error_integral=0;
		static float angle_pitch_error_differen=0, angle_roll_error_differen=0, angle_yaw_error_differen=0;

		//integral and differential values
//		static float gx_integral=0, gy_integral=0, gz_integral=0;
//		float gx_differential, gy_differential, gz_differential;

		//================
		//Data acquisition
		//================
		// MPU_xxxx venían del DMP. En nuestro caso lo estamos obteiendo de un filtro complementario
		// el filtro complementario tiene salida en °, lo paso a radianes
		angle_pitch_estim = pitch_compfilt * M_PI/180.0;
		angle_roll_estim  = roll_compfilt  * M_PI/180.0;
		angle_yaw_estim   = yaw_compfilt   * M_PI/180.0;

		// Esta parte es para el cálculo del término derivativo. Por ahora lo elimino e implemento solo un PI
		gx_filtered = GYR_X * GYRO_SCALE_DEG_TO_RAD;	// Este signo menos está bien puesto
		gy_filtered = GYR_Y * GYRO_SCALE_DEG_TO_RAD;	//
		gz_filtered = GYR_Z * GYRO_SCALE_DEG_TO_RAD;


		//yaw_total is the angle of interest
		yaw_total=angle_yaw_estim;

		//Get Spektrum remote data
		RX_STATUS=CONTROL_GetSpektrumData();



		// Vamos a cambiar ese array angle ref por variables pitch roll y yaw creo, es más legible
		// Lo que se hace es tomar las referencias de la radio y ponerlas en ese array angles_ref
		angle_pitch_ref= Spektrum_angles_ref[0];
		angle_roll_ref = Spektrum_angles_ref[1];
		angle_yaw_ref  = angle_yaw_ref + gz_ref*TS_SAMPLES;	//La referencia de yaw son pequeños incrementos/decrementos de angulo

		//==============
		//PID controller
		//==============



		//Deviations from references
		// El error es la referencia - la medición. Lo corrijo acá respecto de la chori
		angle_pitch_error = angle_pitch_ref - angle_pitch_estim;
		angle_roll_error  = angle_roll_ref  - angle_roll_estim;
		angle_yaw_error   = angle_yaw_ref   - angle_yaw_estim;		//ToDo: Necesito reveer lo referente al yaw

		// Errores integrales. Esto parece estar bien.
		angle_pitch_error_integral = angle_pitch_error_integral + TS_SAMPLES*angle_pitch_error;
		angle_roll_error_integral  = angle_roll_error_integral  + TS_SAMPLES*angle_roll_error;
		angle_yaw_error_integral   = angle_yaw_error_integral   + TS_SAMPLES*angle_yaw_error;

		//Derivative term, should only be executed when remote control data is available, as control loops runs at 200Hz, and remote control send at 50Hz
		// Esto está bien testeado. Aunque el resultado obtenido es feo y por ahora no lo usamos
		if (RX_STATUS==RX_SUCCESS)
		{
			t_end_spektrum=__HAL_TIM_GET_COUNTER(&htim1);
			if (t_end_spektrum>t_ini_spektrum) {
				t_betw_spektrum=t_end_spektrum-t_ini_spektrum;
			}else {
				t_betw_spektrum=t_end_spektrum+(65535-t_ini_spektrum);
			}

			pitch_ref_der = (angle_pitch_ref - pitch_ref_last)/((float)t_betw_spektrum/1000000);
			roll_ref_der  = (angle_roll_ref  - roll_ref_last) /((float)t_betw_spektrum/1000000);

			pitch_ref_last = angle_pitch_ref;
			roll_ref_last  = angle_roll_ref;

			t_ini_spektrum=__HAL_TIM_GET_COUNTER(&htim1);
		}

		// Si uso el valor del giróscopo como la derivada del ángulo, ésta aparece antes que la estimacion del angulo
		// eso me hace pensar que la estimacion esta retrasada y hay que ver por a que se debe. De todos modos
		// acá ppuevo de derivar la estimacion del ángulo. Da valores feos.
		// Devido a todo esto el termino derivativo no lo estoy usando.
//		float angle_pitch_diff, angle_roll_diff;
//		static float angle_roll_prev=0, angle_pitch_prev=0;
//
//		angle_pitch_diff = (angle_pitch_estim - angle_pitch_prev)/TS_SAMPLES;
//		angle_roll_diff  = (angle_roll_estim  - angle_roll_prev) /TS_SAMPLES;
//
//		angle_pitch_prev = angle_pitch_estim;
//		angle_roll_prev  = angle_roll_estim;
//
//		angle_pitch_error_differen = (0 - angle_pitch_diff);
//		angle_roll_error_differen  = (0 - angle_roll_diff);
//		angle_yaw_error_differen   = (0 - gz_filtered);

		// Esta es la otra forma de calcular el termino derivativo
		angle_pitch_error_differen = (0 - gy_filtered);
		angle_roll_error_differen  = (0 - gx_filtered);
		angle_yaw_error_differen   = (0 - gz_filtered);

		// Esto es si uso la referencia en el termino derivativo
//		angle_pitch_error_differen = (pitch_ref_der - gy_filtered);
//		angle_roll_error_differen  = (roll_ref_der  - gx_filtered);
//		angle_yaw_error_differen   = (gz_ref        - gz_filtered);

		//		angle_pitch_error_prev = angle_pitch_error;
//		angle_roll_error_prev = angle_roll_error;

		//Moments in each axis (Gains in continuous time - identical to discrete time because of expression used)
		// Acá corregí los signo, la salida de un controlador estándar es la suma de los 3 aportes.
		// Como tengo el quad atado entre 2 sillas para que solo funcione pitch pongo en 0 los otros momentos.
		Mx = Kp   * angle_roll_error  + Ki   * angle_roll_error_integral  + Kd   * angle_roll_error_differen;
		My = Kp   * angle_pitch_error + Ki   * angle_pitch_error_integral + Kd   * angle_pitch_error_differen;
		Mz = Kp_Y * angle_yaw_error   + Ki_Y * angle_yaw_error_integral   + Kd_Y * angle_yaw_error_differen;

		//Revisar si el signo de Mz va cambiado por tener las ternas como corresponden
		// No tenemos medicion de altura por ahora, así que el joystick va directo ZLMN[0]
		ZLMN[0] = -Z0_joystick;
		ZLMN[1] = Mx;
		ZLMN[2] = My;
		ZLMN[3] = -Mz;

		//QUADCOPTER
		//Torques in each motor
		PWM1_f = pinMQ[0][0]*ZLMN[0] + pinMQ[0][1]*ZLMN[1] + pinMQ[0][2]*ZLMN[2] + pinMQ[0][3]*ZLMN[3];
		PWM2_f = pinMQ[1][0]*ZLMN[0] + pinMQ[1][1]*ZLMN[1] + pinMQ[1][2]*ZLMN[2] + pinMQ[1][3]*ZLMN[3];
		PWM3_f = pinMQ[2][0]*ZLMN[0] + pinMQ[2][1]*ZLMN[1] + pinMQ[2][2]*ZLMN[2] + pinMQ[2][3]*ZLMN[3];
		PWM4_f = pinMQ[3][0]*ZLMN[0] + pinMQ[3][1]*ZLMN[1] + pinMQ[3][2]*ZLMN[2] + pinMQ[3][3]*ZLMN[3];

		//Polinomical approximation
		PWM1_duty=CONTROL_PWMToForce(PWM1_f);
		PWM2_duty=CONTROL_PWMToForce(PWM2_f);
		PWM3_duty=CONTROL_PWMToForce(PWM3_f);
		PWM4_duty=CONTROL_PWMToForce(PWM4_f);

//
//		PWM1_f = Z0_joystick*40 + 100*My;
//		PWM2_f = Z0_joystick*40 + 100*My;
//		PWM3_f = Z0_joystick*40 - 100*My;
//		PWM4_f = Z0_joystick*40 - 100*My;
//
//

		//Enable or disable PWMs
		if (Bench_test==DISABLE) {
			PWM1_duty=0;
			PWM2_duty=0;
			PWM3_duty=0;
			PWM4_duty=0;

//			gx_integral=0;
//			gy_integral=0;
//			gz_integral=0;

			angle_pitch_error_integral=0;
			angle_roll_error_integral=0;
			angle_yaw_error_integral=0;

		}
		//PWM control
		PWM_SetDuty(1, PWM1_duty);
		PWM_SetDuty(2, PWM2_duty);
		PWM_SetDuty(3, PWM3_duty);
		PWM_SetDuty(4, PWM4_duty);

		//Don´t calculate control till next enable
		CONTROL_CALCULATE=DISABLE;
		cnt_Matlab++;
		if(cnt_Matlab == 10)		//20Hz
		{
			cnt_Matlab = 0;

			str_cadena.prefix1 = 'a';
			str_cadena.prefix2 = 'b';
			str_cadena.prefix3 = 'c';
			str_cadena.prefix4 = 'd';
// Pitch data
			str_cadena.p_est = angle_pitch_estim * RAD_TO_DEG;
			str_cadena.p_ref = angle_pitch_ref   * RAD_TO_DEG;
			str_cadena.p_err = angle_pitch_error * RAD_TO_DEG;
			str_cadena.p_ein = angle_pitch_error_integral * RAD_TO_DEG;
			str_cadena.p_ede = angle_pitch_error_differen * RAD_TO_DEG;
// Roll data
			str_cadena.r_est = angle_roll_estim * RAD_TO_DEG;
			str_cadena.r_ref = angle_roll_ref   * RAD_TO_DEG;
			str_cadena.r_err = angle_roll_error * RAD_TO_DEG;
			str_cadena.r_ein = angle_roll_error_integral * RAD_TO_DEG;
			str_cadena.r_ede = angle_roll_error_differen * RAD_TO_DEG;
// Yaw data


			str_cadena.y_est = pitch_a;
			str_cadena.y_ref = pitch_g;
			str_cadena.y_err = pitch_compfilt;
			str_cadena.y_ein = angle_pitch_error_differen * RAD_TO_DEG;
			str_cadena.y_ede = 0;//angle_yaw_error_differen * RAD_TO_DEG;

//			str_cadena.y_est = Kp   * angle_roll_error;
//			str_cadena.y_ref = Kd   * angle_roll_error_differen;
//			str_cadena.y_err = Ki   * angle_roll_error_integral;
//			str_cadena.y_ein = Mx;
//			str_cadena.y_ede = 0;//angle_yaw_error_differen * RAD_TO_DEG;

			str_cadena.r_est = Kp   * angle_pitch_error;
			str_cadena.r_ref = Kd   * angle_pitch_error_differen;
			str_cadena.r_err = Ki   * angle_pitch_error_integral;
			str_cadena.r_ein = My;
			str_cadena.r_ede = 0;//angle_yaw_error_differen * RAD_TO_DEG;
// Momentos
			str_cadena.Mx = Mx;
			str_cadena.My = My;
			str_cadena.Mz = Mz;
// Fuerzas
			str_cadena.PWM1_f = PWM1_f;
			str_cadena.PWM2_f = PWM2_f;
			str_cadena.PWM3_f = PWM3_f;
			str_cadena.PWM4_f = PWM4_f;
// PWM DC
			str_cadena.PWM1 = PWM1_duty/10.0;
			str_cadena.PWM2 = PWM2_duty/10.0;
			str_cadena.PWM3 = PWM3_duty/10.0;
			str_cadena.PWM4 = PWM4_duty/10.0;

			str_cadena.Kp = Kp;
			str_cadena.Ki = Ki;
			str_cadena.Kd = Kd;


			str_cadena.subfix1 = 'u';
			str_cadena.subfix2 = 'v';
			str_cadena.subfix3 = 'w';
			str_cadena.subfix4 = 'x';

			HAL_UART_Transmit_DMA(&huart2,(uint8_t *)&str_cadena, sizeof(str_cadena));	//57 bytes is the max for 115200 and 200Hz loop
//			CDC_Transmit_FS(cadena, 69);
			HAL_GPIO_TogglePin(LED_B_GPIO_Port,LED_B_Pin);
		}

	}
}

float CONTROL_GetSpektrumData(void){

	//obtain remote controller data
	if(rx_get_packet(Spektrum_Data)==RX_SUCCESS)
	{
		Spektrum_roll  =Spektrum_Data[0];		//Channel 1
		Spektrum_height=Spektrum_Data[5];		// Switch on Channel 6
		Spektrum_pitch =Spektrum_Data[1];		// Channel 2
		Spektrum_switch=Spektrum_Data[4];		// Switch on Channel 5
		Spektrum_yaw   =Spektrum_Data[3];		// Channel 4
		Spektrum_thrust=Spektrum_Data[2];		// Channel 3

		//roll stick position to angle
		if ((Spektrum_roll>950)&(Spektrum_roll<2500)) {
			// 1500 - Stick becouse, in normal channel, stick to the right decrease
			// values and we need positive roll reference
			Spektrum_angles_ref[1]=(1500-Spektrum_roll)*MAX_INCLINATION;
		}
		//pitch stick position to angle
		if ((Spektrum_pitch>950)&(Spektrum_pitch<2500)) {
			Spektrum_angles_ref[0]=(Spektrum_pitch-1500)*MAX_INCLINATION;
		}
		//yaw stick position to angle rate
		if ((Spektrum_yaw>950)&(Spektrum_yaw<2500)) {
			if ((Spektrum_yaw>1480)&(Spektrum_yaw<1520)) {
				gz_ref=0;
			}else {
				gz_ref=(1500-Spektrum_yaw)/1000.0;
			}
		}
		//thrust stick position to vertical force
		if ((Spektrum_thrust>950)&(Spektrum_thrust<2500)) {
			//ToDo: Tuve que cambiar de signo este para que funcione bien
			Z0_joystick=(1.0+(Spektrum_thrust-1100)/40.0);
		}

		//on/off switch - Lo invertí respecto a la choriboard
		if ((Spektrum_switch>950)&(Spektrum_switch<2500)) {
			if (Spektrum_switch>1600) {
				Bench_test=DISABLE;
			}else if (Spektrum_switch<1400){
				if (Bench_test==DISABLE) {
					angle_yaw_ref=yaw_total;	//get the current yaw angle as initial reference when switching on (one time only per off-on transition)
				}
				Bench_test=ENABLE;
			}
		}

//		//3-position stick choices, position 1 is default stabilized flight
//		if ((Spektrum_height>1000)&(Spektrum_height<2000)) {
//
//			//--------------------------------------------------
//
//			if ((Spektrum_height>1000)&(Spektrum_height<1750)) {
//
//				//what to do in position 2
//				if (Height_control==DISABLE) {		//only activated when changing control source
//					height_ref=height_filtered/100.0;
//					sesgo_thrust=Z0_joystick;
//				}
//				Height_control=ENABLE;
//			} else {
//				//what to do if not in position 2
//				Height_control=DISABLE;
//			}
//
//			//--------------------------------------------------
//
//			if ((Spektrum_height>1000)&(Spektrum_height<1400)) {
//				//what to do in position 3
//			} else {
//				//what to do if not in position 3
//			}
//
//
//			//--------------------------------------------------
//		}
//		Height_control=DISABLE;
		return RX_SUCCESS;
	}else {
		return RX_NO_PACKET;
	}
}

// Ya estudie esta funcion y ya la puedo usar tal cual la tienen en la chori.
uint16_t CONTROL_PWMToForce(float f){
	uint16_t PWM;
	float PWM_float;
	f=-f;
	PWM_float = (((-0.02406*f + 0.6493)*f + -5.877)*f + 26.93)*f + 7.127;	//Adjust for AIR350, 9545, 4s (aprox 1kg max force, or 10N)
	if (PWM_float<0) {
		PWM=0;
	}else if (PWM_float>100) {
		PWM=1000;
	}else {
		PWM=PWM_float*10;
	}
	return PWM;
}

extern uint8_t recivido[30];
uint8_t tmp=0;
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
//	uint8_t idx=0;
//	while(recivido[idx] != 'M')
//		idx++;
//	idx++;
//	if(recivido[idx] == 'V')
//	{
//		idx++;
//		Kd = recivido[idx]/100.0;
//		idx++;
//		Kp = recivido[idx]/10.0;
//		idx++;
//		Ki = recivido[idx]/10.0;
//		idx++;
//		Kd_Y = recivido[idx]/100.0;
//		idx++;
//		Kp_Y = recivido[idx]/10.0;
//		idx++;
//		Ki_Y = recivido[idx]/10.0;
//	}
//	else
//	{
//		while(1)
//		{
//			HAL_GPIO_TogglePin(LED_R_GPIO_Port,LED_R_Pin);
//			HAL_Delay(500);
//		}
//	}
}
