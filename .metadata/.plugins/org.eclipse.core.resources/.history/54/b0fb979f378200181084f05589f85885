
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "i2c.h"
#include "spi.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
/* InvenSense drivers and utils */
#include "Devices/Drivers/Icm20602/Icm20602.h"
#include "Devices/Drivers/Ak0991x/Ak0991x.h"
#include "Devices/SensorTypes.h"
#include "EmbUtils/Message.h"
#include "EmbUtils/ErrorHelper.h"
#include "EmbUtils/DataConverter.h"
#include "EmbUtils/RingBuffer.h"
#include "DynamicProtocol/DynProtocol.h"
#include "DynamicProtocol/DynProtocolTransportUart.h"

/* InvenSense LibExport */
#include "LibAlgo/LibAlgo.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
static int icm20602_sensor_setup(void);
static int icm20602_sensor_configuration(void);
static int icm20602_run_selftest(void);

static int idd_io_hal_init(void);
static int idd_io_hal_read_reg(void * context, uint8_t reg, uint8_t * rbuffer, uint32_t rlen);
static int idd_io_hal_write_reg(void * context, uint8_t reg, const uint8_t * wbuffer, uint32_t wlen);
static void check_rc(int rc, const char * context_str);
/*
 * Just a handy variable to handle the icm20602 object
 */
static inv_icm20602_t icm_device;

static const uint8_t EXPECTED_WHOAMI[] = { 0x12, 0x11 };  /* WHOAMI value for ICM20602 or derivative */

/* FSR configurations */
static int32_t cfg_acc_fsr = 4000; /* +/- 4g */
static int32_t cfg_gyr_fsr = 2000; /* +/- 2000dps */

/* Sensitivity configurations */
#define ACC_SENSITIVITY (int32_t) ( ((cfg_acc_fsr/1000) * (1 << 16)) / INT16_MAX)
#define GYR_SENSITIVITY (int32_t) ( (cfg_gyr_fsr * (1 << 16) / INT16_MAX) )

/* sensor ODR limit */
#define MIN_ODR_US        1000
#define MAX_ODR_US       20000
#define DEFAULT_ODR_US   20000

/*
 * Variable to keep track of the expected period common for all sensors
 */
static uint32_t period_us = DEFAULT_ODR_US /* 50Hz by default */;

/*
 * Variable keeping track of chip information
 */
static uint8_t chip_info[3];

static struct {
	int32_t acc_cal_q16[3];
	int32_t acc_bias_q16[3];
	uint8_t accuracy_flag;
	union {
		uint8_t buf[ALGO_INVN_CALIBRATION_ACGO_CONFIG_SIZE];
		float   flt; /* ensure correct memory alignment of the buffer */
	} C_buffer;
} sCalAcc;

static struct {
	int32_t gyro_cal_2000dps_q30[3];
	int32_t gyr_cal_q16[3];
	int32_t gyr_uncal_q16[3];
	int32_t gyr_bias_q16[3];
	uint8_t accuracy_flag;
	union {
		uint8_t buf[ALGO_INVN_CALIBRATION_GYR_CAL_FXP_SIZE];
		float   flt; /* ensure correct memory alignment of the buffer */
	} C_buffer;
} sCalGyr;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */


  /*
  * Initialize icm20602 serif structure
  */
  struct inv_icm20602_serif icm20602_serif;

  idd_io_hal_init();
  icm20602_serif.context   = 0; /* no need */
  icm20602_serif.read_reg  = idd_io_hal_read_reg;
  icm20602_serif.write_reg = idd_io_hal_write_reg;

  /*
   * Init SPI communication: SPI1 - SCK(PA5) / MISO(PA6) / MOSI(PA7) / CS(PB6)
   */
  icm20602_serif.max_read  = 1024*32; /* maximum number of bytes allowed per serial read */
  icm20602_serif.max_write = 1024*32; /* maximum number of bytes allowed per serial write */
  icm20602_serif.is_spi    = 1;


	/*
	 * Reset icm20602 driver states
	 */
	inv_icm20602_reset_states(&icm_device, &icm20602_serif);

	/*
	 * Setup the icm20602 device
	 */
	icm20602_sensor_setup();
	icm20602_sensor_configuration();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
int icm20602_sensor_setup(void)
{
	int rc;
	uint8_t i, whoami = 0xff;

	/*
	 * Just get the whoami
	 */
	rc = inv_icm20602_get_whoami(&icm_device, &whoami);
	INV_MSG(INV_MSG_LEVEL_INFO, "ICM20602 WHOAMI=0x%02x", whoami);
	check_rc(rc, "Error reading WHOAMI");

	/*
	 * Check if WHOAMI value corresponds to any value from EXPECTED_WHOAMI array
	 */
	for(i = 0; i < sizeof(EXPECTED_WHOAMI)/sizeof(EXPECTED_WHOAMI[0]); ++i) {
		if(whoami == EXPECTED_WHOAMI[i])
			break;
	}

	if(i == sizeof(EXPECTED_WHOAMI)/sizeof(EXPECTED_WHOAMI[0])) {
		INV_MSG(INV_MSG_LEVEL_ERROR, "Bad WHOAMI value. Got 0x%02x. Expected 0x12, 0x11.", whoami);
		check_rc(-1, "");
	}

	rc = inv_icm20602_get_chip_info(&icm_device, chip_info);
	check_rc(rc, "Could not obtain chip info");

	/*
	 * Configure and initialize the ICM20602 for normal use
	 */
	INV_MSG(INV_MSG_LEVEL_INFO, "Booting up icm20602...");

	/* set default power mode */
	if (!inv_icm20602_is_sensor_enabled(&icm_device, INV_ICM20602_SENSOR_GYRO) &&
		!inv_icm20602_is_sensor_enabled(&icm_device, INV_ICM20602_SENSOR_ACCEL)) {
		INV_MSG(INV_MSG_LEVEL_VERBOSE, "Putting icm20602 in sleep mode...");
		rc = inv_icm20602_initialize(&icm_device);
		check_rc(rc, "Error %d while setting-up icm20602 device");
	}

	/* set default ODR = 50Hz */
	rc = inv_icm20602_set_sensor_period(&icm_device, INV_ICM20602_SENSOR_ACCEL, DEFAULT_ODR_US/1000 /*ms*/);
	check_rc(rc, "Error %d while setting-up icm20602 device");

	rc = inv_icm20602_set_sensor_period(&icm_device, INV_ICM20602_SENSOR_GYRO, DEFAULT_ODR_US/1000 /*ms*/);
	check_rc(rc, "Error %d while setting-up icm20602 device");

	period_us = DEFAULT_ODR_US;

	/* we should be good to go ! */
	INV_MSG(INV_MSG_LEVEL_VERBOSE, "We're good to go !");

	return 0;
}

int icm20602_sensor_configuration(void)
{
	int rc;

	INV_MSG(INV_MSG_LEVEL_INFO, "Configuring accelerometer FSR");
	rc = inv_icm20602_set_accel_fullscale(&icm_device, inv_icm20602_accel_fsr_2_reg(cfg_acc_fsr));
	check_rc(rc, "Error configuring ACC sensor");

	INV_MSG(INV_MSG_LEVEL_INFO, "Configuring gyroscope FSR");
	rc = inv_icm20602_set_gyro_fullscale(&icm_device, inv_icm20602_gyro_fsr_2_reg(cfg_gyr_fsr));
	check_rc(rc, "Error configuring GYR sensor");

	return rc;
}

//static void apply_mouting_matrix(const int32_t mounting_matrix[9], const int16_t raw[3], int32_t out[3])
//{
//	unsigned i;
//
//	for(i = 0; i < 3; i++) {
//		out[i]  = (int32_t)((int64_t)mounting_matrix[3*i+0]*raw[0] >> 30);
//		out[i] += (int32_t)((int64_t)mounting_matrix[3*i+1]*raw[1] >> 30);
//		out[i] += (int32_t)((int64_t)mounting_matrix[3*i+2]*raw[2] >> 30);
//	}
//}

void apply_stored_offsets(void)
{
	uint8_t sensor_bias[84];
	int32_t raw_bias[12] = {0};
	uint8_t i, idx = 0;

	/* Retrieve offsets stored in NV memory */
	if(flash_manager_readData(sensor_bias) != 0) {
		INV_MSG(INV_MSG_LEVEL_WARNING, "No bias values retrieved from NV memory !");
		return;
	}

	for(i = 0; i < 12; i++)
		raw_bias[i] = inv_dc_little8_to_int32((const uint8_t *)(&sensor_bias[i * sizeof(uint32_t)]));
	idx += sizeof(raw_bias);
	inv_icm20602_set_st_bias(&icm_device, (int *)raw_bias);

	for(i = 0; i < 3; i++)
		sCalAcc.acc_bias_q16[i] = inv_dc_little8_to_int32((const uint8_t *)(&sensor_bias[idx + i * sizeof(uint32_t)]));
	idx += sizeof(sCalAcc.acc_bias_q16);

	for(i = 0; i < 3; i++)
		sCalGyr.gyr_bias_q16[i] = inv_dc_little8_to_int32((const uint8_t *)(&sensor_bias[idx + i * sizeof(uint32_t)]));
	idx += sizeof(sCalGyr.gyr_bias_q16);

#if USE_AK09911_MAG
	for(i = 0; i < 3; i++)
		sCalMag.mag_bias_q16[i] = inv_dc_little8_to_int32((const uint8_t *)(&sensor_bias[idx + i * sizeof(uint32_t)]));
#endif
}

int icm20602_run_selftest(void)
{
	int raw_bias[12];
	int rc = 0;

	if (icm_device.selftest_done == 1) {
		INV_MSG(INV_MSG_LEVEL_INFO, "Self-test has already ran. Skipping.");
	}
	else {
		/*
		 * Perform self-test
		 * For ICM20602 self-test is performed for both RAW_ACC/RAW_GYR
		 */
		INV_MSG(INV_MSG_LEVEL_INFO, "Running self-test...");

		/* Run the self-test */
		rc = inv_icm20602_run_selftest(&icm_device);
		/* Check transport errors */
		check_rc(rc, "Self-test failure");
		if (rc != 0x3) {
			/*
			 * Check for GYR success (1 << 0) and ACC success (1 << 1),
			 * but don't block as these are 'usage' failures.
			 */
			INV_MSG(INV_MSG_LEVEL_ERROR, "Self-test failure");
			/* 0 would be considered OK, we want KO */
			return INV_ERROR;
		} else
			/* On success, offset will be kept until reset */
			icm_device.selftest_done = 1;

		/* It's advised to re-init the icm20602 device after self-test for normal use */
		rc = icm20602_sensor_setup();
	}

	/*
	 * Get Low Noise / Low Power bias computed by self-tests scaled by 2^16
	 */
	INV_MSG(INV_MSG_LEVEL_INFO, "Getting LP/LN bias");
	inv_icm20602_get_st_bias(&icm_device, raw_bias);
	INV_MSG(INV_MSG_LEVEL_INFO, "GYR LN bias (FS=250dps) (dps): x=%f, y=%f, z=%f",
			(float)(raw_bias[0] / (float)(1 << 16)), (float)(raw_bias[1] / (float)(1 << 16)), (float)(raw_bias[2] / (float)(1 << 16)));
	INV_MSG(INV_MSG_LEVEL_INFO, "GYR LP bias (FS=250dps) (dps): x=%f, y=%f, z=%f",
			(float)(raw_bias[3] / (float)(1 << 16)), (float)(raw_bias[4] / (float)(1 << 16)), (float)(raw_bias[5] / (float)(1 << 16)));
	INV_MSG(INV_MSG_LEVEL_INFO, "ACC LN bias (FS=2g) (g): x=%f, y=%f, z=%f",
			(float)(raw_bias[0 + 6] / (float)(1 << 16)), (float)(raw_bias[1 + 6] / (float)(1 << 16)), (float)(raw_bias[2 + 6] / (float)(1 << 16)));
	INV_MSG(INV_MSG_LEVEL_INFO, "ACC LP bias (FS=2g) (g): x=%f, y=%f, z=%f",
			(float)(raw_bias[3 + 6] / (float)(1 << 16)), (float)(raw_bias[4 + 6] / (float)(1 << 16)), (float)(raw_bias[5 + 6] / (float)(1 << 16)));

	return rc;
}
/*
 * Sleep implementation for ICM20602
 */
void inv_icm20602_sleep(int ms)
{
//	delay_ms(ms);
	HAL_Delay(ms);
}
void inv_icm20602_sleep_us(int us)
{
//	delay_us(us);
	HAL_Delay(us);
}
/*
 * Helper function to check RC value and block programm exectution
 */
static void check_rc(int rc, const char * msg_context)
{
	if(rc < 0) {
		INV_MSG(INV_MSG_LEVEL_ERROR, "%s: error %d (%s)", msg_context, rc, inv_error_str(rc));
		while(1);
	}
}
/******************************************************************************/
/* Low-level serial inteface function implementation                          */
/******************************************************************************/

/* Icm20602 Serif object definition for SPI/I2C **************************/

static int idd_io_hal_init(void)
{
//	spi_master_init(SPI_NUM1, SPI_6MHZ);
	return 0;
}

static int idd_io_hal_read_reg(void * context, uint8_t reg, uint8_t * rbuffer, uint32_t rlen)
{
	(void)context;
//	return spi_master_read_register(SPI_NUM1, reg, rlen, rbuffer);
}

static int idd_io_hal_write_reg(void * context, uint8_t reg, const uint8_t * wbuffer, uint32_t wlen)
{
	(void)context;
//	return spi_master_write_register(SPI_NUM1, reg, wlen, wbuffer);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
