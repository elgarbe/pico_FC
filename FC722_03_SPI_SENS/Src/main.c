
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "spi.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "math.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
#define CMD_RESET    0x1E  // ADC reset command
#define CMD_ADC_READ 0x00  // ADC read command
#define CMD_ADC_CONV 0x40  // ADC conversion command
#define CMD_ADC_D1   0x00  // ADC D1 conversion
#define CMD_ADC_D2   0x10  // ADC D2 conversion
#define CMD_ADC_256  0x00  // ADC OSR=256
#define CMD_ADC_512  0x02  // ADC OSR=512
#define CMD_ADC_1024 0x04  // ADC OSR=1024
#define CMD_ADC_2048 0x06  // ADC OSR=2056
#define CMD_ADC_4096 0x08  // ADC OSR=4096
#define CMD_PROM_RD  0xA0  // Prom read command

void cmd_reset(void)
{
       HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_RESET);		// pull CSB low to start the command
       HAL_SPI_Transmit(&hspi1,(uint8_t *)CMD_RESET,1,100);					// send reset sequence
       HAL_Delay(3);														// wait for the reset sequence timing
       HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the command
}

uint32_t cmd_prom(char coef_num)
{
       uint8_t ret[3];
       uint8_t code[3] = {CMD_PROM_RD+coef_num*2, 0, 0};
       uint32_t rC=0;
       HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start the command
       // send PROM READ command
       HAL_SPI_TransmitReceive(&hspi1, code, ret, 3,100);	// send reset sequence
       rC=ret[1]*256 + ret[2];
       HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the command
       return rC;
}

unsigned long cmd_adc(uint8_t cmd)
{
	uint8_t ret[4];
	uint8_t code[4] = {0, 0, 0, 0};
	uint8_t res;
	uint8_t com;
	unsigned long temp=0;

	com = CMD_ADC_CONV+cmd;
	HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start the command
	HAL_SPI_TransmitReceive(&hspi1, &com, &res, 1,100);					// send conversion command
	HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the conversion
	switch (cmd & 0x0f)													// wait necessary conversion time
	{
		case CMD_ADC_256 : HAL_Delay(1); break;
		case CMD_ADC_512 : HAL_Delay(3);   break;
		case CMD_ADC_1024: HAL_Delay(4);   break;
		case CMD_ADC_2048: HAL_Delay(6);   break;
		case CMD_ADC_4096: HAL_Delay(10);  break;
	}

	HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
    HAL_SPI_TransmitReceive(&hspi1, code, ret, 4,500);	// send ADC read command
    temp=ret[1]*65536 + ret[2]*256 + ret[3];
    HAL_GPIO_WritePin(PRES_CS_GPIO_Port, PRES_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command
    return temp;
}

unsigned char crc4(uint16_t n_prom[])
{
       int cnt;                                        // simple counter
       unsigned int n_rem;                             // crc reminder
       unsigned int crc_read;                          // original value of the crc
       unsigned char  n_bit;
       n_rem = 0x00;
       crc_read=n_prom[7];                             //save read CRC
       n_prom[7]=(0xFF00 & (n_prom[7]));               //CRC byte is replaced by 0
       for (cnt = 0; cnt < 16; cnt++)                 // operation is performed on bytes
       {      // choose LSB or MSB
              if (cnt%2==1) n_rem ^= (unsigned short) ((n_prom[cnt>>1]) & 0x00FF);
              else n_rem ^= (unsigned short) (n_prom[cnt>>1]>>8);
             for (n_bit = 8; n_bit > 0; n_bit--)
             {
                     if (n_rem & (0x8000))
                     {
                        n_rem = (n_rem << 1) ^ 0x3000;
                     }
                     else
                     {
                        n_rem = (n_rem << 1);
                     }
             }
       }
       n_rem=  (0x000F & (n_rem >> 12)); // // final 4-bit reminder is CRC code
       n_prom[7]=crc_read;               // restore the crc_read to its original place
       return (n_rem ^ 0x00);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint16_t C[8]; // calibration coefficients
    unsigned long D1;          // ADC value of the pressure conversion
    unsigned long D2;          // ADC value of the temperature conversion
    double P;                  // compensated pressure value
    double T;                  // compensated temperature value
    double dT;                 // difference between actual and measured temperature
    double OFF;                // offset at actual temperature
    double SENS;                // sensitivity at actual temperature

	uint8_t i;
	unsigned char n_crc; // crc value of the prom
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
  cmd_reset();								//reset the module after powerup
  for (i=0;i<8;i++){ 						//read calibration coefficients
	  C[i]=cmd_prom(i);
  }
  n_crc=crc4(C);
  // Check CRC of data read
  if( (C[7] & 0xF) != n_crc )
  {
	  while(1);
  }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  D1=cmd_adc(CMD_ADC_D1+CMD_ADC_256);     // read uncompensated pressure
	  D2=cmd_adc(CMD_ADC_D2+CMD_ADC_256);    // read uncompensated temperature
      // calcualte 1st order pressure and temperature (MS5607 1st order algorithm)
      dT=D2-C[5]*pow(2,8);
      OFF=C[2]*pow(2,16)+dT*C[4]/pow(2,7);
      SENS=C[1]*pow(2,15)+dT*C[3]/pow(2,8);
      T=(2000+(dT*C[6])/pow(2,23))/100;
      P=(((D1*SENS)/pow(2,21)-OFF)/pow(2,15))/100;
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
