/*
 * main.cpp

 *
 *  Created on: 2018/01/17
 *      Author: yoneken
 */
#include <mainpp.h>
#include <ros.h>
#include <std_msgs/String.h>

ros::NodeHandle nh;

std_msgs::String str_msg;
ros::Publisher chatter("chatter", &str_msg);
char hello[] = "GISE!!!";

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
  nh.getHardware()->flush();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  nh.getHardware()->reset_rbuf();
}

void setup(void)
{
  nh.initNode();
  nh.advertise(chatter);
}
uint32_t lasttime = 0;
void loop(void)
{
  HAL_GPIO_TogglePin(LED_B_GPIO_Port, LED_B_Pin);

  str_msg.data = hello;
  chatter.publish(&str_msg);
  nh.spinOnce();

  HAL_Delay(500);
//	  if (HAL_GetTick() - lasttime > 50)
//	  {
//		  str_msg.data = hello;
//		  chatter.publish( &str_msg );
//		  lasttime = HAL_GetTick();
//	  }
//	  nh.spinOnce();
}

