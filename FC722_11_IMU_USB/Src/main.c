
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "icm20602.h"
#include "usbd_cdc_if.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//uint16_t tim_values[100];
//int16_t ACC_X=0;
//int16_t ACC_Y=0;
//int16_t ACC_Z=0;
//int16_t GYR_X=0;
//int16_t GYR_Y=0;
//int16_t GYR_Z=0;
//int16_t TEMP=0;
float ACC_X=0.0;
float ACC_Y=0.0;
float ACC_Z=0.0;
float GYR_X=0.0;
float GYR_Y=0.0;
float GYR_Z=0.0;
float TEMP=0.0;

float pitch_g=0.0;
float roll_g=0.0;
float pitch_a=0.0;
float roll_a=0.0;
float pitch_compfilt=0.0;
float roll_compfilt=0.0;
float yaw=0.0;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint8_t cadena[100];
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
//
//  uint8_t ret[7];
//  uint8_t code[7]={0,0,0,0,0,0,0};
//
//
//  uint8_t data=0;

  icm_config();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint8_t * p_data;
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
//	  sprintf((char *)cadena,"%04.2f, %04.2f, %04.2f, %05.2f, %06.2f, %06.2f, %06.2f\n\r",
//			  ACC_X, ACC_Y, ACC_Z, TEMP, GYR_X, GYR_Y, GYR_Z);
//	  CDC_Transmit_FS(cadena, 51);
//	  sprintf((char *)cadena,"P_a: %04.2f, R_a: %04.2f, P_g: %04.2f, R_g: %05.2f\n\r",
//			  pitch_a, roll_a, pitch_g, roll_g);
	  cadena[0] = 'a';
	  cadena[1] = 'b';
	  cadena[2] = 'c';
	  cadena[3] = 'd';

		p_data = (uint8_t*)&pitch_a;
		cadena[4] = *p_data;
		p_data++;
		cadena[5] = *p_data;
		p_data++;
		cadena[6] = *p_data;
		p_data++;
		cadena[7] = *p_data;

		p_data = (uint8_t*)&roll_a;
		cadena[8] = *p_data;
		p_data++;
		cadena[9] = *p_data;
		p_data++;
		cadena[10] = *p_data;
		p_data++;
		cadena[11] = *p_data;

		p_data = (uint8_t*)&pitch_g;
		cadena[12] = *p_data;
		p_data++;
		cadena[13] = *p_data;
		p_data++;
		cadena[14] = *p_data;
		p_data++;
		cadena[15] = *p_data;

		p_data = (uint8_t*)&roll_g;
		cadena[16] = *p_data;
		p_data++;
		cadena[17] = *p_data;
		p_data++;
		cadena[18] = *p_data;
		p_data++;
		cadena[19] = *p_data;

		p_data = (uint8_t*)&pitch_compfilt;
		cadena[20] = *p_data;
		p_data++;
		cadena[21] = *p_data;
		p_data++;
		cadena[22] = *p_data;
		p_data++;
		cadena[23] = *p_data;

		p_data = (uint8_t*)&roll_compfilt;
		cadena[24] = *p_data;
		p_data++;
		cadena[25] = *p_data;
		p_data++;
		cadena[26] = *p_data;
		p_data++;
		cadena[27] = *p_data;
		cadena[28] = 'u';

	  CDC_Transmit_FS(cadena, 29);
	  HAL_GPIO_TogglePin(LED_B_GPIO_Port,LED_B_Pin);
	  HAL_Delay(20);
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 6;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/*

 //  code[0] = 26;
//  code[1] = 0;
//
//  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
//  HAL_SPI_Transmit(&hspi1, code, 2,500);	// send ADC read command
//  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command
//
//  HAL_Delay(100);

  //Read WHO AM I
  code[0] = 128+117;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 2,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

  if(ret[1] != 0x12)
  {
	  while(10)
	  {
		  HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);
	  }
  }

  //Selft test accelerometer
  //Leo los 3 ejes del acelerómetro
  code[0] = 128+59;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 7,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command
  ACC_X = (256.0 * ret[1] + ret[2]);
  ACC_Y = (256.0 * ret[3] + ret[4]);
  ACC_Z = (256.0 * ret[5] + ret[6]);

  //Leo los 3 valores almacenados como valor de ST para cada eje X, Y y Z
  code[0] = 13+128; code[1] = 0; code[2] = 0; code[3] = 0; code[4] = 0;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 4,500);
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command
  ACC_X_st = ret[1];
  ACC_Y_st = ret[2];
  ACC_Z_st = ret[3];

  code[0] = 28;					//Activo el self test
  code[1] = 0b11100000;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_Transmit(&hspi1, code, 2,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

  code[0] = 28+128;					//Probando +-4g
  code[1] = 0;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 2,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command


  //Leo lo nuevos valores de aceleracion con el ST activado
  code[0] = 128+59;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 7,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command
  ACC_X2 = (256.0 * ret[1] + ret[2]);
  ACC_Y2 = (256.0 * ret[3] + ret[4]);
  ACC_Z2 = (256.0 * ret[5] + ret[6]);



  code[0] = 28;					//Probando +-4g
  code[1] = 0b00000000;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_Transmit(&hspi1, code, 2,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

  code[0] = 28+128;					//Probando +-4g
  code[1] = 0;
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command
  HAL_SPI_TransmitReceive(&hspi1, code, ret, 2,500);	// send ADC read command
  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

 */
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
