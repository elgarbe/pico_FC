

#include "main.h"
#include "stm32f7xx_hal.h"
#include "spi.h"
#include "string.h"
#include "tim.h"
#include "math.h"
#include "../fc_drivers/icm20602.h"

#define ALPHA	0.025

//Buffer to request Acc, Temp and Gyro Data
//We need to transmit 1 byte to write reg address, then 14 0s to put clock pulses to read data back from sensor
uint8_t data_add[15] = {MPUREG_ACCEL_XOUT_H+128,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
//Buffer to store Accel, Temp and Gyro data byte
uint8_t data_reg[15];

//extern int16_t ACC_X;
//extern int16_t ACC_Y;
//extern int16_t ACC_Z;
//extern int16_t GYR_X;
//extern int16_t GYR_Y;
//extern int16_t GYR_Z;
//extern int16_t TEMP;
extern float ACC_X;
extern float ACC_Y;
extern float ACC_Z;
extern float GYR_X;
extern float GYR_Y;
extern float GYR_Z;
extern float TEMP;

extern float pitch_g;
extern float roll_g;
extern float pitch_a;
extern float roll_a;
//extern float yaw;
extern float pitch_compfilt;
extern float roll_compfilt;


int icm_write_reg(uint8_t reg, uint8_t * wbuffer, uint32_t wlen)
{
	HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command

	HAL_SPI_Transmit(&hspi1, &reg, 1,500);				// write register address
	HAL_SPI_Transmit(&hspi1, wbuffer, wlen,500);		// write register values

	HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

	return 0;
}
int icm_read_reg(uint8_t reg, uint8_t * rbuffer, uint32_t rlen)
{
	reg |= 128;					// Read bit mask of register address
	uint8_t zeros[rlen];		// Create an array filled with 0 for spi transmit.
	memset(zeros, 0, rlen);		//

	HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start new command

	HAL_SPI_Transmit(&hspi1, &reg, 1,500);							// Write address
	HAL_SPI_TransmitReceive(&hspi1, zeros, rbuffer, rlen,500);		// read registers

	HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);		// pull CSB high to finish the read command

	return 0;
}

void icm_config(void)
{
	  uint8_t data=0;

	  //0x6B - PWR_MGMT_1 - Reset IMU
	  data=1;
	  icm_write_reg(MPUREG_PWR_MGMT_1, &data, 1);
	  HAL_Delay(100);

	  //0x19 - SMPLRT_DIV - 200Hz = 1000Hz / (4+1)
	  data=4;
	  icm_write_reg(MPUREG_SMPLRT_DIV, &data, 1);

	  //0x1A - CONFIG - FIFO=0, EXT_SYNC=0, DLPF_CFG=1
	  data=0b00000001;
	  icm_write_reg(MPUREG_CONFIG, &data, 1);

	  //0x1B - Gyro configured by default +-250dps
	  data=0;
	  icm_write_reg(MPUREG_GYRO_CONFIG, &data, 1);
	  //0x1C - Accel configured by default +-2g
	  icm_write_reg(MPUREG_ACCEL_CONFIG, &data, 1);
	  //0x1D - Accel config 2 configured by default
	  icm_write_reg(MPUREG_ACCEL_CONFIG_2, &data, 1);
	  //0x1E - Gyro LP_MODE configured by default
	  icm_write_reg(MPUREG_LP_CONFIG, &data, 1);
	  //0x37 INT_PIN_CFG - Interrupt status clear in any data reading
	  data=BIT_INT_RD_CLEAR | BIT_INT_LEVEL | BIT_INT_OPEN;
	  icm_write_reg(MPUREG_INT_PIN_CFG, &data, 1);


	  //0x70 - I2C_IF - Disable i2c interface
	  data=0b01000000;
	  icm_write_reg(0x70, &data, 1);

	  //Read WHO AM I
	  icm_read_reg(MPUREG_WHO_AM_I,&data,1);
	  if(data != 0x12)
	  {
		  while(1)
		  {
			  HAL_GPIO_TogglePin(LED_R_GPIO_Port, LED_R_Pin);
		  }
	  }

	  HAL_NVIC_DisableIRQ(EXTI4_IRQn);
	  int16_t dato=0;

	  icm_write_reg(MPUREG_XG_OFFS_USRH, (uint8_t *)&dato,2 );
	  icm_write_reg(MPUREG_YG_OFFS_USRH, (uint8_t *)&dato,2 );
	  icm_write_reg(MPUREG_ZG_OFFS_USRH, (uint8_t *)&dato,2 );

	  //Obtain sensor offset's
#define OFFS_SAMPLES	200
	  uint8_t cant_samples=OFFS_SAMPLES;
	  int32_t raw_acc_x=0;
	  int32_t raw_acc_y=0;
	  int32_t raw_acc_z=0;
	  int32_t raw_gyr_x=0;
	  int32_t raw_gyr_y=0;
	  int32_t raw_gyr_z=0;
	  // We colect OFFS_SAMPLES samples from accelerometer and gyroscope to make an average of them
	  // that avg is considered the BIAS or OFFSET of the sensors. icm20602 have registers to store it
	  while(cant_samples)
	  {
		  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start comm
		  HAL_SPI_TransmitReceive(&hspi1, data_add, data_reg, 15,100);
		  HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);	// pull CSB high to finish comm

		  raw_acc_x += (int16_t)(data_reg[ 1]*256 + data_reg[2]);
		  raw_acc_y += (int16_t)(data_reg[ 3]*256 + data_reg[4]);
		  raw_acc_z += (int16_t)(data_reg[ 5]*256 + data_reg[6]);
		  raw_gyr_x += (int16_t)(data_reg[ 9]*256 + data_reg[10]);
		  raw_gyr_y += (int16_t)(data_reg[11]*256 + data_reg[12]);
		  raw_gyr_z += (int16_t)(data_reg[13]*256 + data_reg[14]);
		  cant_samples--;
		  HAL_Delay(10);
	  }
	  raw_acc_x /= OFFS_SAMPLES;
	  raw_acc_y /= OFFS_SAMPLES;
	  raw_acc_z /= OFFS_SAMPLES;
	  raw_gyr_x /= OFFS_SAMPLES;
	  raw_gyr_y /= OFFS_SAMPLES;
	  raw_gyr_z /= OFFS_SAMPLES;

	  raw_gyr_x = -raw_gyr_x/4;
	  raw_gyr_y = -raw_gyr_y/4;
	  raw_gyr_z = -raw_gyr_z/4;

//	  icm_read_reg(MPUREG_XG_OFFS_USRH, data_reg,6);

	  dato = ((raw_gyr_x & 0x0000ff00) >> 8) | (raw_gyr_x << 8);
	  icm_write_reg(MPUREG_XG_OFFS_USRH, (uint8_t *)&dato,2 );
	  dato = ((raw_gyr_y & 0x0000ff00) >> 8) | (raw_gyr_y << 8);
	  icm_write_reg(MPUREG_YG_OFFS_USRH, (uint8_t *)&dato,2 );
	  dato = ((raw_gyr_z & 0x0000ff00) >> 8) | (raw_gyr_z << 8);
	  icm_write_reg(MPUREG_ZG_OFFS_USRH, (uint8_t *)&dato,2 );

//	  icm_read_reg(MPUREG_XG_OFFS_USRH, data_reg,6);
	  // once we store the sensor's offsets can enable Data Ready interrupt and start getting data in DMA
	  // and interrupt mode
	  //0x38 INT_ENABLE - The fucking datasheet doesn't show this bit in page 41!!!
	  data=BIT_DATA_RDY_INT_EN;
	  icm_write_reg(MPUREG_INT_ENABLE, &data, 1);

	  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}
//uint16_t tim_value=0;
void icm20602_start_dma_receive(void)
{
//		HAL_GPIO_TogglePin(LED_G_GPIO_Port,LED_G_Pin);
//		tim_value = __HAL_TIM_GetCounter(&htim1) - tim_value;
		HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_RESET);	// pull CSB low to start comm
		HAL_SPI_TransmitReceive_DMA(&hspi1, data_add, data_reg, 15);
}

void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
	if(hspi1.Instance == SPI1)
	{
		HAL_GPIO_WritePin(MPU_CS_GPIO_Port, MPU_CS_Pin,GPIO_PIN_SET);	// pull CSB high to finish comm
		// Store data readed into data variables
		ACC_X = (int16_t)(data_reg[ 1]*256 + data_reg[2])  /16384.0;
		ACC_Y = (int16_t)(data_reg[ 3]*256 + data_reg[4])  /16384.0;
		ACC_Z = (int16_t)(data_reg[ 5]*256 + data_reg[6])  /16384.0;
		TEMP  = (int16_t)(data_reg[ 7]*256 + data_reg[8])  /326.8+25.0;
		GYR_X = (int16_t)(data_reg[ 9]*256 + data_reg[10]) /131.0;
		GYR_Y = (int16_t)(data_reg[11]*256 + data_reg[12]) /131.0;
		GYR_Z = (int16_t)(data_reg[13]*256 + data_reg[14]) /131.0;

		//Gyros angles. angle_previo = angle_previo + dps_gyro * Ts (ó 1/Fs)
		pitch_g += (GYR_Y / 200.0);
		roll_g  += (GYR_X / 200.0);
		// There are 2 o 3 differnt formula here.
		pitch_a = atan2(-ACC_X, sqrt(ACC_Y*ACC_Y + ACC_Z*ACC_Z)) * 180.0/M_PI;
		roll_a = atan2(ACC_Y, ACC_Z) * 180.0/M_PI;
		//Complementary Filter
		pitch_compfilt = (1.0 - ALPHA) * (pitch_compfilt + GYR_Y / 200.0) + pitch_a * ALPHA;
		roll_compfilt  = (1.0 - ALPHA) * (roll_compfilt  + GYR_X / 200.0) + roll_a  * ALPHA;

	}

}
