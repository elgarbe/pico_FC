/*
 * spektrum.h
 *
 *  Created on: 17 jul. 2018
 *      Author: elgarbe
 */

#ifndef SPEKTRUM_H_
#define SPEKTRUM_H_

#define RX_CHANNELS		8

uint8_t rx_get_packet(uint32_t packet[RX_CHANNELS]);
void spektrum_read_channel(void);

#endif /* SPEKTRUM_H_ */
