/*
 * controller.c
 *
 *  Created on: 19 jul. 2018
 *      Author: elgarbe
 */
#include "usbd_cdc_if.h"
#include "controller.h"
#include "spektrum.h"

extern float pitch_compfilt;
extern float roll_compfilt;

extern float GYR_X;
extern float GYR_Y;
extern float GYR_Z;

uint8_t cnt_Matlab=0;

FunctionalState CONTROL_CALCULATE = DISABLE;

// Las hice globales para poder mostrarlas en el main
float angle_pitch_error, angle_roll_error, angle_yaw_error;

//Spektrum controller
uint32_t Spektrum_Data[6]={0,0,0,0,0,0};
uint16_t Spektrum_pitch=0;
uint16_t Spektrum_roll=0;
uint16_t Spektrum_yaw=0;
uint16_t Spektrum_thrust=0;
uint16_t Spektrum_switch=0;
uint16_t Spektrum_height=0;

//Controller references
float Spektrum_angles_ref[3]={0,0,0};
static float angles_ref[3]={0,0,0};
float gz_ref=0;

//Variables with main use inside controller but modified sometimes in external functions
float Z0_joystick=0;

//PID Pitch-Roll-Yaw
float K_D=0.5;		//diff (prop gyro)
float K_P=1.4;		//prop angle
float K_I=1.0;		//int angle

float K_PHI=0.0;	//int gyro
float K_DIF=0.00;	//diff gyro

float K_D_YAW=0.5;
float K_P_YAW=0.5;
float K_I_YAW=0.2;

void CONTROL_PID(void)
{
	uint8_t cadena[100];
	uint8_t * p_data;

	if (CONTROL_CALCULATE==ENABLE) {
		float RX_STATUS;
		//Moments in each axis
		float Mx, My, Mz;
		static float pitch_ref_der, roll_ref_der;

		//========================================================================================================================================
		//========================================================================================================================================
		//pinMQ Matrix: moments to torque; ZLMN: moments vector; torques in each motor
		//====================================================================
	/*
		//Hexacopter DJI F550 standard, kt=0.03
		static float pinMQ[6][4]={	{0.1667,   -0.6061,    1.0497,    5.5556},
									{0.1667,    0.6061,    1.0497,   -5.5556},
									{0.1667,    1.2121,    0.0000,    5.5556},
									{0.1667,    0.6061,   -1.0497,   -5.5556},
									{0.1667,   -0.6061,   -1.0497,    5.5556},
									{0.1667,   -1.2121,   -0.0000,   -5.5556}};
	*/
	/*
		//Hexacopter DJI F550, 73° tilted inwards, kt=0.03
		static float pinMQ[6][4]={	{0.1743,   -0.5965,    1.1176,    5.8094},
									{0.1743,    0.5965,    1.1176,   -5.8094},
									{0.1743,    1.2661,   -0.0422,    5.8094},
									{0.1743,    0.6696,   -1.0754,   -5.8094},
									{0.1743,   -0.6696,   -1.0754,    5.8094},
									{0.1743,   -1.2661,   -0.0422,   -5.8094}};
	*/

		//Quadcopter GPSIC, kt=0.03
		static float pinMQ[4][4]={	{0.2500,   -1.4142,    1.4142,    8.3333},
									{0.2500,    1.4142,    1.4142,   -8.3333},
									{0.2500,    1.4142,   -1.4142,    8.3333},
									{0.2500,   -1.4142,   -1.4142,   -8.3333}};



		//========================================================================================================================================
		//========================================================================================================================================
		static float ZLMN[4]={0, 0, 0, 0};
		float PWM1_f, PWM2_f, PWM3_f, PWM4_f, PWM5_f, PWM6_f;

		//Estimated angles using gyros integral and accel data with different weights
		static float angle_pitch_estim=0, angle_roll_estim=0;

		static float gx_filtered=0, gy_filtered=0, gz_filtered=0;
		static float gx_filtered_last=0, gy_filtered_last=0, gz_filtered_last=0;

		// Las hice globales para poder mostrarlas en el main
	//	float angle_pitch_error, angle_roll_error, angle_yaw_error;

		//integral and differential values
		static float gx_integral=0, gy_integral=0, gz_integral=0;
		float gx_differential, gy_differential, gz_differential;
		static float angle_pitch_error_integral=0, angle_roll_error_integral=0, angle_yaw_error_integral;

		//================
		//Data acquisition
		//================
		// MPU_xxxx venían del DMP. En nuestro caso lo estamos obteiendo de un filtro complementario
		angle_pitch_estim = pitch_compfilt;
		angle_roll_estim  = roll_compfilt;
	//	angle_pitch_estim = MPU_pitch;
	//	angle_roll_estim  = MPU_roll;

		gx_filtered =  GYR_X * GYRO_SCALE_DEG_TO_RAD;	//	ToDo: Revisar estos signos
		gy_filtered = -GYR_Y * GYRO_SCALE_DEG_TO_RAD;	//
		gz_filtered = -GYR_Z * GYRO_SCALE_DEG_TO_RAD;
	//	gx_filtered =  ((gyroX) / GYRO_SCALE) * GYRO_SCALE_DEG_TO_RAD;	//apparently the dmp autocorrects gyro bias when set through init calibration
	//	gy_filtered = -((gyroY) / GYRO_SCALE) * GYRO_SCALE_DEG_TO_RAD;	//no need to substract it here
	//	gz_filtered = -((gyroZ) / GYRO_SCALE) * GYRO_SCALE_DEG_TO_RAD;

		//Get Spektrum remote data
		RX_STATUS=CONTROL_GetSpektrumData();

		angles_ref[0]=Spektrum_angles_ref[0];
		angles_ref[1]=Spektrum_angles_ref[1];
		angles_ref[2]=angles_ref[2]+gz_ref*TS_SAMPLES;

		//==============
		//PID controller
		//==============

		//ToDo: Esto ya lo estoy calculando en la IMU: pitch_g, roll_g
		//Integral gyro values
		gx_integral = gx_integral + TS_SAMPLES*gx_filtered;
		gy_integral = gy_integral + TS_SAMPLES*gy_filtered;
		gz_integral = gz_integral + TS_SAMPLES*gz_filtered;

		gx_differential = (gx_filtered - gx_filtered_last)/TS_SAMPLES;
		gy_differential = (gy_filtered - gy_filtered_last)/TS_SAMPLES;
		gz_differential = (gz_filtered - gz_filtered_last)/TS_SAMPLES;

		gx_filtered_last = gx_filtered;
		gy_filtered_last = gy_filtered;
		gz_filtered_last = gz_filtered;

		//Deviations from references
		angle_pitch_error = angle_pitch_estim + angles_ref[0];
		angle_roll_error  = angle_roll_estim  + angles_ref[1];
		//ToDo: Necesito reveer lo referente al yaw
		angle_yaw_error   = 0                 - angles_ref[2];
	//	angle_yaw_error = yaw_total-angles_ref[2];

		angle_pitch_error_integral = angle_pitch_error_integral + TS_SAMPLES*angle_pitch_error;
		angle_roll_error_integral = angle_roll_error_integral + TS_SAMPLES*angle_roll_error;
		angle_yaw_error_integral = angle_yaw_error_integral + TS_SAMPLES*angle_yaw_error;


		//ToDo: Tengo que reveer este código
		//Derivative term, should only be executed when remote control data is available, as control loops runs at 200Hz, and remote control send at 50Hz
		if (RX_STATUS==RX_SUCCESS) {
	//		t_end_spektrum=TS_Get(TIM0);
	//		if (t_end_spektrum>t_ini_spektrum) {
	//			t_betw_spektrum=t_end_spektrum-t_ini_spektrum;
	//		}else {
	//			t_betw_spektrum=1000000+t_end_spektrum-t_ini_spektrum;
	//		}
	//
	//		pitch_ref_der = (angles_ref[0] - pitch_ref_last)/((float)t_betw_spektrum/1000000);
	//		roll_ref_der = (angles_ref[1] - roll_ref_last)/((float)t_betw_spektrum/1000000);
	//		pitch_ref_last=angles_ref[0];
	//		roll_ref_last=angles_ref[1];
	//
	//		t_ini_spektrum=TS_Get(TIM0);
		}

		//Moments in each axis (Gains in continuous time - identical to discrete time because of expression used)
		Mx = - K_D     * (gx_filtered-roll_ref_der)  - K_P     * angle_roll_error    - K_I     * angle_roll_error_integral;
		My = - K_D     * (gy_filtered+pitch_ref_der) - K_P     * angle_pitch_error   - K_I     * angle_pitch_error_integral;
		Mz =   K_D_YAW * (gz_filtered-gz_ref)        + K_P_YAW * angle_yaw_error     + K_I_YAW * angle_yaw_error_integral;

		//Revisar si el signo de Mz va cambiado por tener las ternas como corresponden
		ZLMN[1] = -Mx;
		ZLMN[2] = -My;
		ZLMN[3] =  Mz;


		//Don´t calculate control till next enable
		CONTROL_CALCULATE=DISABLE;

		if(cnt_Matlab++ == 20)
		{
			cnt_Matlab = 0;
			cadena[0] = 'a';
			cadena[1] = 'b';
			cadena[2] = 'c';
			cadena[3] = 'd';

			p_data = (uint8_t*)&pitch_compfilt;
			cadena[4] = *p_data;
			p_data++;
			cadena[5] = *p_data;
			p_data++;
			cadena[6] = *p_data;
			p_data++;
			cadena[7] = *p_data;

			p_data = (uint8_t*)&Spektrum_angles_ref[0];
			cadena[8] = *p_data;
			p_data++;
			cadena[9] = *p_data;
			p_data++;
			cadena[10] = *p_data;
			p_data++;
			cadena[11] = *p_data;

			p_data = (uint8_t*)&angle_pitch_error;
			cadena[12] = *p_data;
			p_data++;
			cadena[13] = *p_data;
			p_data++;
			cadena[14] = *p_data;
			p_data++;
			cadena[15] = *p_data;

			p_data = (uint8_t*)&angle_pitch_error_integral;
			cadena[16] = *p_data;
			p_data++;
			cadena[17] = *p_data;
			p_data++;
			cadena[18] = *p_data;
			p_data++;
			cadena[19] = *p_data;

			p_data = (uint8_t*)&roll_compfilt;
			cadena[20] = *p_data;
			p_data++;
			cadena[21] = *p_data;
			p_data++;
			cadena[22] = *p_data;
			p_data++;
			cadena[23] = *p_data;

			p_data = (uint8_t*)&Spektrum_angles_ref[1];
			cadena[24] = *p_data;
			p_data++;
			cadena[25] = *p_data;
			p_data++;
			cadena[26] = *p_data;
			p_data++;
			cadena[27] = *p_data;

			p_data = (uint8_t*)&angle_roll_error;
			cadena[28] = *p_data;
			p_data++;
			cadena[29] = *p_data;
			p_data++;
			cadena[30] = *p_data;
			p_data++;
			cadena[31] = *p_data;

			p_data = (uint8_t*)&angle_roll_error_integral;
			cadena[32] = *p_data;
			p_data++;
			cadena[33] = *p_data;
			p_data++;
			cadena[34] = *p_data;
			p_data++;
			cadena[35] = *p_data;

			cadena[36] = 'u';

			CDC_Transmit_FS(cadena, 37);
			HAL_GPIO_TogglePin(LED_B_GPIO_Port,LED_B_Pin);
		}

	}
}

float CONTROL_GetSpektrumData(void){

	//obtain remote controller data
	if(rx_get_packet(Spektrum_Data)==RX_SUCCESS){
		Spektrum_roll  =Spektrum_Data[0];		//Channel 1
		Spektrum_height=Spektrum_Data[5];		// Switch on Channel 6
		Spektrum_pitch =Spektrum_Data[1];		// Channel 2
		Spektrum_switch=Spektrum_Data[4];		// Switch on Channel 5
		Spektrum_yaw   =Spektrum_Data[3];		// Channel 4
		Spektrum_thrust=Spektrum_Data[2];		// Channel 3

		//roll stick position to angle
		if ((Spektrum_roll>950)&(Spektrum_roll<2500)) {
			Spektrum_angles_ref[1]=(Spektrum_roll-1500)*MAX_INCLINATION;
//			Spektrum_angles_ref[1]=(1500-Spektrum_roll)*MAX_INCLINATION;
		}
		//pitch stick position to angle
		if ((Spektrum_pitch>950)&(Spektrum_pitch<2500)) {
			Spektrum_angles_ref[0]=(Spektrum_pitch-1500)*MAX_INCLINATION;
		}
		//yaw stick position to angle rate
		if ((Spektrum_yaw>950)&(Spektrum_yaw<2500)) {
			if ((Spektrum_yaw>1480)&(Spektrum_yaw<1520)) {
				gz_ref=0;
			}else {
				gz_ref=(1500-Spektrum_yaw)/400.0;
			}
		}
		//thrust stick position to vertical force
		if ((Spektrum_thrust>950)&(Spektrum_thrust<2500)) {
			Z0_joystick=-(1.0+(Spektrum_thrust-1100)/25.0);
		}

//		//on/off switch
//		if ((Spektrum_switch>950)&(Spektrum_switch<2500)) {
//			if (Spektrum_switch<1200) {
//				Bench_test=DISABLE;
//			}else if (Spektrum_switch>1800){
//				if (Bench_test==DISABLE) {
//					angles_ref[2]=yaw_total;	//get the current yaw angle as initial reference when switching on (one time only per off-on transition)
//				}
//				Bench_test=ENABLE;
//			}
//		}
//
//		//3-position stick choices, position 1 is default stabilized flight
//		if ((Spektrum_height>1000)&(Spektrum_height<2000)) {
//
//			//--------------------------------------------------
//
//			if ((Spektrum_height>1000)&(Spektrum_height<1750)) {
//
//				//what to do in position 2
//				if (Height_control==DISABLE) {		//only activated when changing control source
//					height_ref=height_filtered/100.0;
//					sesgo_thrust=Z0_joystick;
//				}
//				Height_control=ENABLE;
//			} else {
//				//what to do if not in position 2
//				Height_control=DISABLE;
//			}
//
//			//--------------------------------------------------
//
//			if ((Spektrum_height>1000)&(Spektrum_height<1400)) {
//				//what to do in position 3
//			} else {
//				//what to do if not in position 3
//			}
//
//
//			//--------------------------------------------------
//		}
//		Height_control=DISABLE;
		return RX_SUCCESS;
	}else {
		return RX_NO_PACKET;
	}
}
