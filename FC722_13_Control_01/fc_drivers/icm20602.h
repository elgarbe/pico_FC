
#ifndef ICN20602_H_
#define ICN20602_H_

#include "../fc_drivers/Icm20602Defs.h"

int icm_write_reg(uint8_t reg, uint8_t * wbuffer, uint32_t wlen);
int icm_read_reg(uint8_t reg, uint8_t * rbuffer, uint32_t rlen);
void icm_config(void);
void icm20602_start_dma_receive(void);

#endif /* SPEKTRUM_H_ */
