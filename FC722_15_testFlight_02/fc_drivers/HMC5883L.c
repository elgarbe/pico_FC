/*
 * HMC5883L.c
 *
 *  Created on: 12/11/2013
 *      Author: Claudio
 */

#include "main.h"
#include "HMC5883L.h"
#include "i2c.h"


extern int16_t Mag_Data[3];

void MAG_Initialize(void){

	uint8_t data[3]={0,0,0};

	  data[0]=MAG_REG_IDA;
	  data[1]=0;
	  HAL_I2C_Master_Transmit(&hi2c1, MAG_Address, data, 1, 100);
	  // Data returnes is H 4 3
	  HAL_I2C_Master_Receive(&hi2c1, MAG_Address+1, data, 3, 100);

	//REG A config
	data[0]=MAG_REG_CFG_A;
	data[1]=MAG_REG_CFG_A_SET;
	MAG_Write(data,2);

	//REG B config
	data[0]=MAG_REG_CFG_B;
	data[1]=MAG_REG_CFG_B_SET;
	MAG_Write(data,2);
}

void MAG_Write(uint8_t* data, uint16_t length)
{

	HAL_I2C_Master_Transmit(&hi2c1, MAG_Address, data, length, 100);
}

void MAG_Read(uint8_t* data, uint8_t MemoryRead, uint16_t length){

	HAL_I2C_Master_Transmit(&hi2c1, MAG_Address, &MemoryRead, 1, 100);
	HAL_I2C_Master_Receive(&hi2c1, MAG_Address+1, data, length, 100);
}

//Once DATA RDY Interrupt is triggered we read mag data and fill the array
void MAG_Read_MagData(void)
{
	uint8_t mag_buffer[6];
	//Read data from MAG
	MAG_Read(mag_buffer, MAG_REG_X_H, 6);

	//receive X, then Z, then Y
	// aligned with IMU X is -X and Y is -Y
	Mag_Data[0] =  ((mag_buffer[0]<<8)|mag_buffer[1]);
	Mag_Data[1] = -((mag_buffer[4]<<8)|mag_buffer[5]);
	Mag_Data[2] = -((mag_buffer[2]<<8)|mag_buffer[3]);
	HAL_GPIO_TogglePin(LED_G_GPIO_Port, LED_G_Pin);
}

// 50Hz timer for Magnetometer measurement. Could I use 75Hz from magnetometer? why use a timer?
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance == TIM4)
	{
		MAG_One_Measurement();
	}
}

// Ask to magnetometer to make a conversion
void MAG_One_Measurement(void)
{
	uint8_t data[2]={0,0};
	//Mode Reg - Single measurement
	data[0]=MAG_REG_MODE;
	data[1]=MAG_REG_MODE_SINGLE_MEASURE;
	MAG_Write(data,2);
}
