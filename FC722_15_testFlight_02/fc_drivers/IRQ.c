/*
 * IRQ.c
 *
 *  Created on: 8 ago. 2018
 *      Author: elgarbe
 */

#include "main.h"
#include "../fc_drivers/spektrum.h"
#include "../fc_drivers/icm20602.h"
#include "../fc_drivers/HMC5883L.h"

// External Interrupt ISR Callback
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	// internal IMU EOC
	if(GPIO_Pin == EOC_IMU_Pin)
	{
		icm20602_start_dma_receive();
	}
	// If the IRQ coming from RC_PPM pin
	if(GPIO_Pin == RC_PPM_Pin)
	{
		spektrum_read_channel();
	}
	// If the IRQ coming from Magnetometer EOC
	if(GPIO_Pin == EOC_MAG_Pin)
	{
		MAG_Read_MagData();
	}
}

