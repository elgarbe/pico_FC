/*
 * controller.h
 *
 *  Created on: 19 jul. 2018
 *      Author: elgarbe
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "stm32f7xx_hal.h"

//scale to pass from degrees to rads (2*pi)/360
#define GYRO_SCALE_DEG_TO_RAD 0.0174533

#define RAD_TO_DEG 180.0/M_PI
#define DEG_TO_RAD M_PI/180.0
//Pitch and roll max inclination = max_inc° * pi/180 * 1/500
#define MAX_INCLINATION 0.0008727	//25°
//#define MAX_INCLINATION 0.0013963	//40°
//#define MAX_INCLINATION 0.0017453	//50°
//#define MAX_INCLINATION 0.1		// 50°/500

//Samples of IMU between controls and time separation
#define CONTROL_SAMPLES	10
//acá los comentarios no parecen estar bien. En este caso TS_SAMPLE es 200Hz igual que la IMU
#define TS_SAMPLES 0.005			//(CONTROL_SAMPLES/IMU_sampling_rate)

float CONTROL_GetSpektrumData(void);					//get controller stick and switches position
void CONTROL_PID(void);									//Calculates PID control and modifies PWM duty
uint16_t CONTROL_PWMToForce(float f);					//force to PWM conversion, motor/propeller dependant
void CONTROL_AdjustMagnetometer(void);

#endif /* CONTROLLER_H_ */
