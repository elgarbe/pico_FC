/*
 * HMC5883L.h
 *
 *  Created on: 12/11/2013
 *      Author: Claudio
 */

#ifndef HMC5883L_H_
#define HMC5883L_H_

#include "stm32f7xx_hal.h"

//MAG address - 0x3C Write / 0x3D Read
#define MAG_Address 0x3C	//last bit changes if reading or writing

//MAG registers
#define MAG_REG_CFG_A	0
#define MAG_REG_CFG_B	1
#define MAG_REG_MODE	2
//ALERT!! X then Z then Y, not what expected!
#define MAG_REG_X_H		3
#define MAG_REG_X_L		4
#define MAG_REG_Z_H		5
#define MAG_REG_Z_L		6
#define MAG_REG_Y_H		7
#define MAG_REG_Y_L		8
#define MAG_REG_STAT	9
#define MAG_REG_IDA		10
#define MAG_REG_IDB		11
#define MAG_REG_IDC		12

//MAG register configurations
//1 samples averaged, 75Hz output, normal measurement
#define MAG_REG_CFG_A_SET 0b00011000
//FRS=0.88Ga
#define MAG_REG_CFG_B_SET 0b00000000
//Start single measure
#define MAG_REG_MODE_SINGLE_MEASURE 0b00000001

void MAG_Initialize(void);									//Inits Magnetometer interface
void I2C_Initialize(void);									//Inits I2C interface
void MAG_One_Measurement(void);

void MAG_Write(uint8_t* data, uint16_t length);				//Writes data of (length) bytes
void MAG_Read(uint8_t* data, uint8_t MemoryRead, uint16_t length);		//Reads and stores in data of (length) bytes starting in MemoryRead address

#endif /* HMC5883L_H_ */
