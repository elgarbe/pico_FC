/*
 * PWM.c
 *
 *  Created on: 20 jul. 2018
 *      Author: elgarbe
 */

#include "tim.h"
#include "PWM.h"


void PWM_Initialize(void)
{
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_Base_Start(&htim3);
	PWM_SetDuty(1, 0);
	PWM_SetDuty(2, 0);
	PWM_SetDuty(3, 0);
	PWM_SetDuty(4, 0);
	PWM_SetDuty(5, 0);
	PWM_SetDuty(6, 0);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_4);
}

void PWM_SetDuty(uint8_t channel, uint16_t duty)
{

	switch (channel)
	{
	case 1:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, ESC_MIN_PWM_TIME+duty);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME+duty, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, ESC_MIN_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_3, ESC_MAX_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MAX_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);
		}
		break;
	case 2:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, ESC_MIN_PWM_TIME+duty);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME+duty, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, ESC_MIN_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_4, ESC_MAX_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MAX_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);
		}
		break;
	case 3:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, ESC_MIN_PWM_TIME+duty);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME+duty, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, ESC_MIN_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, ESC_MAX_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MAX_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);
		}
		break;
	case 4:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, ESC_MIN_PWM_TIME+duty);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME+duty, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, ESC_MIN_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MIN_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, ESC_MAX_PWM_TIME);
//			PWM_MatchUpdate(LPC_PWM1, channel, ESC_MAX_PWM_TIME, PWM_MATCH_UPDATE_NEXT_RST);
		}
		break;
	case 5:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, ESC_MIN_PWM_TIME+duty);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, ESC_MIN_PWM_TIME);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, ESC_MAX_PWM_TIME);
		}
		break;
	case 6:
		if ((duty<=DUTY_MAX)&(duty>=DUTY_MIN)){
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, ESC_MIN_PWM_TIME+duty);

		}else if (duty<DUTY_MIN){		//if duty is less than 0, min pulse width is 1mS
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, ESC_MIN_PWM_TIME);

		}else if (duty>DUTY_MAX) {
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, ESC_MAX_PWM_TIME);
		}
		break;
	}
}
