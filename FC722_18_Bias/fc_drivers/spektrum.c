/*
 * spektrum.c
 *
 *  Created on: 17 jul. 2018
 *      Author: elgarbe
 */



#include "main.h"
#include "stm32f7xx_hal.h"
#include "tim.h"
#include "gpio.h"
#include "../fc_drivers/spektrum.h"

/* variables to be updated at the ISR */
volatile static uint32_t pulses[RX_CHANNELS];
volatile static uint8_t packet_available;

void spektrum_read_channel(void)
{
	static uint32_t tmp_pulses[RX_CHANNELS];
	static uint32_t prev_ticks = 0;
	static uint32_t current_channel = 0;
	uint32_t curr_ticks, nanos, c;

	// Get current timer count
	curr_ticks = __HAL_TIM_GET_COUNTER(&htim1);
	// Get delta time between two IRQ
	// If the IRQ coming from RC_PPM pin
	if(HAL_GPIO_ReadPin(RC_PPM_GPIO_Port,RC_PPM_Pin)==GPIO_PIN_RESET)
	{
		// Get current timer count
		curr_ticks = __HAL_TIM_GET_COUNTER(&htim1);
		// Get delta time between two IRQ
		if (curr_ticks > prev_ticks)
		{
			nanos = curr_ticks - prev_ticks;
		}else
		{
			nanos = curr_ticks + (65535 - prev_ticks);
		}
		prev_ticks = curr_ticks;
		// Check for long puls (sync channels with that pulse)
		if(nanos > 3000)
		{
			current_channel=0;	// Reset channel counter
		}
		else
		{
			tmp_pulses[current_channel] = nanos; 	/* store this sample */
			current_channel++;						// Increment channel index counter
			if (current_channel == RX_CHANNELS)		/* check for end of packet */
			{
				current_channel = 0;				// Reset channel index counter
				for (c=0; c<RX_CHANNELS; c++)		// copy tmp_pulses into global pulses array
					pulses[c] = tmp_pulses[c];
				packet_available = 1;				// flag to indicate the reception of a packet of RX data
			}
		}
	}
}

// rx_get_packet():
//		If a packet has arrived since the last call, 'packet' is filled with
//		the contents of the packet, and RX_SUCCESS is returned; otherwise,
//		RX_NO_PACKET is returned. The size of 'packet' must be at least RX_CHANNELS.
uint8_t rx_get_packet(uint32_t packet[RX_CHANNELS])
{
	uint32_t avail, ch;

	HAL_NVIC_DisableIRQ(EXTI2_IRQn);
	avail = packet_available; 		/* IRQ-updated flag */
	packet_available = 0;
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);

	// packet available?
	if (avail)
	{
		for (ch = 0; ch < RX_CHANNELS; ch++)
			packet[ch] = pulses[ch];
		return RX_SUCCESS;
	}
	else {
		return RX_NO_PACKET;
	}
}
