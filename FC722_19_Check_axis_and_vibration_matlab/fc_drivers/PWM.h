/*
 * PWM.h
 *
 *  Created on: 20 jul. 2018
 *      Author: elgarbe
 */

#ifndef PWM_H_
#define PWM_H_

#include "stm32f7xx_hal.h"

#define ESC_CONTROL_PERIOD		2500 		//At 400 Hz, time between control is 2500 uS
//#define ESC_CONTROL_PERIOD	10000 		//At 100 Hz, time between control is 10000 uS
#define ESC_MIN_PWM_TIME 		1000		//PWM width should be between 1 and 2 mS
#define ESC_MAX_PWM_TIME 		2000
#define DUTY_MIN				0					//Duty goes between 0 and 1000 (0-100%)
#define DUTY_MAX				1000


void PWM_SetDuty(uint8_t channel, uint16_t duty);		//Modifies Duty cycle of the channel, duty may be between 0 and 1000
void PWM_Initialize(void);


#endif /* PWM_H_ */
