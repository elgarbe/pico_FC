/*
 * spektrum.h
 *
 *  Created on: 17 jul. 2018
 *      Author: elgarbe
 */

#ifndef SPEKTRUM_H_
#define SPEKTRUM_H_

#include "stm32f7xx_hal.h"

#define RX_CHANNELS		8		/* number of PWM channels OR'ed at the input of the Capture module */
#define RX_SUCCESS		0
#define RX_NO_PACKET	1

uint8_t rx_get_packet(uint32_t packet[RX_CHANNELS]);
void spektrum_read_channel(void);

#endif /* SPEKTRUM_H_ */
