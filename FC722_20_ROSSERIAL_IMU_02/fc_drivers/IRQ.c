/*
 * IRQ.c
 *
 *  Created on: 8 ago. 2018
 *      Author: elgarbe
 */

#include "main.h"
#include "../fc_drivers/icm20602.h"
#include "stm32f7xx_hal.h"

// External Interrupt ISR Callback
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	// internal IMU EOC
	if(GPIO_Pin == EOC_IMU_Pin)
	{
		icm20602_start_dma_receive();
	}
}

