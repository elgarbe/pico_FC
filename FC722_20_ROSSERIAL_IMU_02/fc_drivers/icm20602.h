
#ifndef ICM20602_H_
#define ICM20602_H_
#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f7xx_hal.h"
#include "../fc_drivers/Icm20602Defs.h"

int icm_write_reg(uint8_t reg, uint8_t * wbuffer, uint32_t wlen);
int icm_read_reg(uint8_t reg, uint8_t * rbuffer, uint32_t rlen);
void icm_config(void);
void icm20602_start_dma_receive(void);

#ifdef __cplusplus
}
#endif
#endif /* SPEKTRUM_H_ */
