#ifndef _ROS_att_control_choriros_MomentsStamped_h
#define _ROS_att_control_choriros_MomentsStamped_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "std_msgs/Header.h"

namespace att_control_choriros
{

  class MomentsStamped : public ros::Msg
  {
    public:
      typedef std_msgs::Header _header_type;
      _header_type header;
      typedef float _L_type;
      _L_type L;
      typedef float _M_type;
      _M_type M;
      typedef float _N_type;
      _N_type N;
      typedef float _Z_type;
      _Z_type Z;

    MomentsStamped():
      header(),
      L(0),
      M(0),
      N(0),
      Z(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->header.serialize(outbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_L;
      u_L.real = this->L;
      *(outbuffer + offset + 0) = (u_L.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_L.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_L.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_L.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->L);
      union {
        float real;
        uint32_t base;
      } u_M;
      u_M.real = this->M;
      *(outbuffer + offset + 0) = (u_M.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_M.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_M.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_M.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->M);
      union {
        float real;
        uint32_t base;
      } u_N;
      u_N.real = this->N;
      *(outbuffer + offset + 0) = (u_N.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_N.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_N.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_N.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->N);
      union {
        float real;
        uint32_t base;
      } u_Z;
      u_Z.real = this->Z;
      *(outbuffer + offset + 0) = (u_Z.base >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (u_Z.base >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (u_Z.base >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (u_Z.base >> (8 * 3)) & 0xFF;
      offset += sizeof(this->Z);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->header.deserialize(inbuffer + offset);
      union {
        float real;
        uint32_t base;
      } u_L;
      u_L.base = 0;
      u_L.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_L.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_L.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_L.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->L = u_L.real;
      offset += sizeof(this->L);
      union {
        float real;
        uint32_t base;
      } u_M;
      u_M.base = 0;
      u_M.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_M.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_M.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_M.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->M = u_M.real;
      offset += sizeof(this->M);
      union {
        float real;
        uint32_t base;
      } u_N;
      u_N.base = 0;
      u_N.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_N.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_N.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_N.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->N = u_N.real;
      offset += sizeof(this->N);
      union {
        float real;
        uint32_t base;
      } u_Z;
      u_Z.base = 0;
      u_Z.base |= ((uint32_t) (*(inbuffer + offset + 0))) << (8 * 0);
      u_Z.base |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      u_Z.base |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      u_Z.base |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      this->Z = u_Z.real;
      offset += sizeof(this->Z);
     return offset;
    }

    const char * getType(){ return "att_control_choriros/MomentsStamped"; };
    const char * getMD5(){ return "fd7517746d0c277cdab5914fa16269c0"; };

  };

}
#endif