clear all;
vuelo=load('vuelo02.mat');
data=vuelo.ans.Data;
data2=squeeze(data);

% % PITCH Data
p_est=data2(1,:);
p_ref=data2(2,:);


% p_err=data2(3,:);
% p_ein=data2(4,:);
% p_ede=data2(5,:);
% % ROLL data
r_est=data2(6,:);
r_ref=data2(7,:);
% r_err=data2(8,:);
% r_ein=data2(9,:);
% r_ede=data2(10,:);
% % YAW data
y_est=data2(11,:);
y_ref=data2(12,:);
% y_err=data2(13,:);
% y_ein=data2(14,:);
% y_ede=data2(15,:);
% 
% p_ini=200;
% p_end=600;
% Ts=0.05;
% t_ini=p_ini*Ts;
% t_end=p_end*Ts;
% t=t_ini:Ts:t_end;
% fig1 = figure(1);
% fig1.Position=[403 246 560 460];
% plot(t, p_est(p_ini:p_end)-1.2,'b--',t, p_ref(p_ini:p_end),'b');
% title('Pitch References and measured angle')
% xlabel('Time [s]')          % x-axis label
% ylabel('Orientation [°]')   % y-axis label
% legend('pitch - measured','pitch - reference')
% 
% 
% 
% 
% p_ini=300;
% p_end=700;
% Ts=0.05;
% t_ini=p_ini*Ts;
% t_end=p_end*Ts;
% t=t_ini:Ts:t_end;
% fig2 = figure(2);
% fig2.Position=[403 246 560 460];
% plot(t, r_est(p_ini:p_end)-1.2,'b--',t, r_ref(p_ini:p_end),'b');
% title('Roll References and measured angle')
% xlabel('Time [s]')          % x-axis label
% ylabel('Orientation [°]')   % y-axis label
% legend('roll - measured','roll - reference')
% 
% p_ini=2600;
% p_end=3500;
% Ts=0.05;
% t_ini=p_ini*Ts;
% t_end=p_end*Ts;
% t=t_ini:Ts:t_end;
% fig3 = figure(3);
% fig3.Position=[403 246 560 460];
% plot(t, y_est(p_ini:p_end)-1.2,'b--',t, y_ref(p_ini:p_end),'b');
% title('Yaw References and measured angle')
% xlabel('Time [s]')          % x-axis label
% ylabel('Orientation [°]')   % y-axis label
% legend('yaw - measured','yaw - reference')
% 
plot(r_est)
hold on
plot(r_ref)
